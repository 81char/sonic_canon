# -*- mode: sonic-pi -*-
#   _____                          _         _____
#  / ____|                        (_)       |  __ \
# | |     __ _ _ __   ___  _ __    _ _ __   | |  | |
# | |    / _` | '_ \ / _ \| '_ \  | | '_ \  | |  | |
# | |___| (_| | | | | (_) | | | | | | | | | | |__| |
#  \_____\__,_|_| |_|\___/|_| |_| |_|_| |_| |_____/


# -----------------------/////////////////
# Kanon und Gigue für 3 Violinen mit Generalbaß
# Canon and Gigue for 3 violions with basso continuo
# Johann Pachelbel (1653-1706)
# music score based on Max Seiffert (1868-1948)'s edition (public domain) and Jawher Matmati's edition (CC-BY-NC-SA 4.0)
# source: https://imslp.org/wiki/Canon_and_Gigue_in_D_major%2C_P.37_(Pachelbel%2C_Johann)
# Sonic Pi code by Chen W. Zhu, Jessica Duckworth, Keren Simpkins and Cecelia Xinhang Sui (College of Arts and Law, University of Birmingham) July 2023
# Code licence: GNU General Public License 3.0
# /////////////////---------------------------

longa = 16 # long; c0000 (lilypond only) ==>  c\longa
breve = 8 # double whole; breve; c00 (lilypond only) ==> c\breve
dw = 6 # dotted whole note
w = 4 # whole note; semibreve
dh = 3 # dotted half note
h = 2 # half note; minim
dq = 1.5 # dotted quarter note
q = 1 # quarter note; crotchet
de = 0.75 # dotted eighth note
e = 0.5 # eighth note; quaver
s = 0.25 # sixteenth note; semiquaver
dsq = 0.125 # thirty second note; demisemiquiver semiquaver (I am using the british shorthand here)
t1 = 1.0/3 # triplet in 1 beats
t2 = 2.0/3 # triplet in 2 beats
t4 = 4.0/3 # triplet in 4 beats
# insert a blank commented line to make yala-sonic-pi work properly
use_bpm 56

# Violin 1
in_thread do
  use_synth :piano
  # First violin by Jessica Duckworth
  
  play_pattern_timed [:r,:r], [w,w]   # bars1-2
  play_pattern_timed [:fs5, :e5, :d5, :cs5], [q, q, q, q]  # bar 3
  play_pattern_timed [:b4, :a4, :b4, :cs5], [q, q, q, q]   # bar 4
  play_pattern_timed [:d5, :cs5, :b4, :a4], [q, q, q, q]   # bar 5
  play_pattern_timed [:g4, :fs4, :g4, :e4], [q, q, q, q]   # bar 6
  play_pattern_timed [:d4, :fs4, :a4, :g4, :fs4, :d4, :fs4, :e4], [e, e, e, e, e, e, e, e]   # bar 7
  play_pattern_timed [:d4, :b3, :d4, :a4, :g4, :b4, :a4, :g4], [e, e, e, e, e, e, e, e]   # bar 8
  play_pattern_timed [:fs4, :d4, :e4, :cs5, :d5, :fs5, :a5, :a4], [e, e, e, e, e, e, e, e]   # bar 9
  play_pattern_timed [:b4, :g4, :a4, :fs4, :d4, :d5, :d5, :cs5], [e, e, e, e, e, e, de, s]   # bar 10
  play_pattern_timed [:d5, :cs5, :d5, :d4, :cs4, :a4, :e4, :fs4, :d4, :d5, :cs5, :b4, :cs5, :fs5, :a5, :b5], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 11
  play_pattern_timed [:g5, :fs5, :e5, :g5, :fs5, :e5, :d5, :cs5, :b4, :a4, :g4, :fs4, :e4, :g4, :fs4, :e4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 12
  play_pattern_timed [:d4, :e4, :fs4, :g4, :a4, :e4, :a4, :g4, :fs4, :b4, :a4, :g4, :a4, :g4, :fs4, :e4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 13
  play_pattern_timed [:d4, :b3, :b4, :cs5, :d5, :cs5, :b4, :a4, :g4, :fs4, :e4, :b4, :a4, :b4, :a4, :g4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 14
  play_pattern_timed [:fs4, :fs5, :e5, :r, :d5, :fs5], [e, e, q, e, e, q]   # bar 15
  play_pattern_timed [:b5, :a5, :b5, :cs6], [q, q, q, q]   # bar 16
  play_pattern_timed [:d6, :d5, :cs5, :r, :b4, :d5], [e, e, q, e, e, q]   # bar 17
  play_pattern_timed [:d5, :d5, :d5, :g5, :e5, :a5], [dq, e, e, e, e, e]   # bar 18
  play_pattern_timed [:a5, :fs5, :g5, :a5, :fs5, :g5, :a5, :a4, :b4, :cs5, :d5, :e5, :fs5, :g5, :fs5, :d5, :e5, :fs5, :fs4, :g4, :a4, :b4, :a4, :g4, :a4, :fs4, :g4, :a4], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]   # bar 19
  play_pattern_timed [:g4,  :b4, :a4, :g4, :fs4, :e4, :fs4, :e4, :d4, :e4, :fs4, :g4, :a4, :b4, :g4, :b4, :a4, :b4, :cs5, :d5, :a4, :b4, :cs5, :d5, :e5, :fs5, :g5, :a5], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]   # bar 20
  play_pattern_timed [:fs5, :d5, :e5, :fs5, :e5, :d5, :e5, :cs5, :d5, :e5, :fs5, :e5, :d5, :cs5, :d5, :b4, :cs5, :d5, :d4, :e4, :fs4, :g4, :fs4, :e4, :fs4, :d5, :cs5, :d5], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]   # bar 21
  play_pattern_timed [:b4, :d5, :cs5, :b4, :a4, :g4, :a4, :g4, :fs4, :g4, :a4, :b4, :cs5, :d5, :b4, :d5, :cs5, :d5, :cs5, :b4, :cs5, :d5, :e5, :d5, :cs5, :d5, :b4, :cs5], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]   # bar 22
  play_pattern_timed [:d5, :r, :cs5, :r, :b4, :r, :d5, :r], [e, e, e, e, e, e, e, e]   # bar 23
  play_pattern_timed [:d4, :r, :d4, :r, :d4, :r, :e4, :r], [e, e, e, e, e, e, e, e]   # bar 24
  play_pattern_timed [:r, :a4, :r, :a4, :r, :fs4, :r, :a4], [e, e, e, e, e, e, e, e]   # bar 25
  play_pattern_timed [:r, :g4, :r, :fs4, :r, :g4, :r, :e5], [e, e, e, e, e, e, e, e]   # bar 26
  play_pattern_timed [:fs5, :fs4, :g4, :fs4, :e4, :e5, :fs5, :e5, :d5, :fs4, :d4, :b4, :a4, :a3, :g3, :a3], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 27
  play_pattern_timed [:b3, :b4, :cs5, :b4, :a4, :a3, :g3, :a3, :b3, :b4, :a4, :b4, :cs5, :cs4, :b3, :cs4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 28
  play_pattern_timed [:d4, :d5, :e5, :d5, :cs5, :cs4, :d4, :cs4, :b3, :b4, :a4, :b4, :cs5, :cs4, :fs4, :e4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 29
  play_pattern_timed [:d4, :d5, :e5, :g5, :fs5, :fs4, :a4, :fs5, :d5, :g5, :fs5, :g5, :e5, :a4, :g4, :a4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 30
  play_pattern_timed [:fs4, :a4, :a4, :a4, :a4, :a4, :a4, :a4, :fs4, :fs4, :fs4, :fs4, :fs4, :fs4, :a4, :a4], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 31
  play_pattern_timed [:g4, :g4, :g4, :d5, :d5, :d5, :d5, :d5, :d5, :d5, :b4, :b4, :a4, :a4, :e5, :cs5], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 32
  play_pattern_timed [:a4, :fs5, :fs5, :fs5, :e5, :e5, :e5, :e5, :d5, :d5, :d5, :d5, :a5, :a5, :a5, :a5], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 33
  play_pattern_timed [:b5, :b5, :b5, :b5, :a5, :a5, :a5, :a5, :b5, :b5, :b5, :b5, :cs6, :cs5, :cs5, :cs5], [s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s]   # bar 34
  play_pattern_timed [:d5, :d4, :e4, :fs4, :d4, :cs4, :cs5, :d5, :e5, :cs5, :b4, :b3, :cs4, :d4, :b3, :cs4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s]   # bar 35
  play_pattern_timed [:d4, :g4, :fs4, :e4, :g4, :fs4, :d4, :e4, :fs4, :a4, :g4, :b4, :a4, :g4, :fs4, :e4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s]   # bar 36
  play_pattern_timed [:fs4, :d5, :cs5, :d5, :fs4, :a4, :a4, :b4, :cs5, :a4, :fs4, :d5, :e5, :fs5, :d5, :fs5, :fs5, :e5, :d5, :cs5], [s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s]   # bar 37
  play_pattern_timed [:b4, :b4, :a4, :b4, :cs5, :d5, :fs5, :e5, :d5, :fs5, :g5, :d5, :cs5, :b4, :b4, :a4, :e4, :a4, :a4], [s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, s, s, s]   # bar 38
  play_pattern_timed [:a4, :a4, :d4, :a4], [dq, e, dq, e]   # bar 39
  play_pattern_timed [:g4, :a4, :g4 ,:d4, :d4, :cs4], [q, q, e, e, de, s]   # bar 40
  play_pattern_timed [:d4, :d5, :cs5, :b4, :a4], [e, e, q, q, q]   # bar 41
  play_pattern_timed [:d4, :e4, :fs4, :b4, :e4, :e4], [de, s, q, q, de, s]   # bar 42
  play_pattern_timed [:fs4, :fs5, :fs5, :g5, :fs5, :e5, :d5, :d5, :d5, :e5, :d5, :cs5], [de, s, s, s, s, s, de, s, s, s, s, s]   # bar 43
  play_pattern_timed [:b4, :d5, :d5, :c5, :b4, :c5, :a4, :a4], [q, q, s, s, s, s, de, s]   # bar 44
  play_pattern_timed [:a4, :a5, :a5, :b5, :a5, :g5, :fs5, :fs5, :fs5, :g5, :fs5, :e5], [de, s, s, s, s, s, de, s, s, s, s, s]   # bar 45
  play_pattern_timed [:d5, :c5, :b4, :c5, :a4, :a4, :g4, :d5, :cs5, :cs5], [s, s, s, s, de, s, e, e, de, s]   # bar 46
  play_pattern_timed [:d5, :d5, :cs5, :b4, :a4], [e, q, q, q, e]   # bar 47
  play_pattern_timed [:a4, :g4, :fs4, :fs4, :e4, :e4], [e, q, e, de, s, q]   # bar 48
  play_pattern_timed [:fs4, :fs5, :e5, :d5, :d6, :c6], [e, q, e, e, q, e]   # bar 49
  play_pattern_timed [:b5, :d6, :a5, :b5, :a5], [q, e, e, q, q]   # bar 50
  play_pattern_timed [:a5, :a4, :g4, :fs4, :fs5, :e5], [q, de, s, q, de, s]   # bar 51
  play_pattern_timed [:d5, :d5, :d5, :cs5], [dq, e, q, q]   # bar 52
  play_pattern_timed [:d5, :d4, :cs4, :cs5, :b4, :b3, :a3, :a4], [e, e, e, e, e, e, e, e]   # bar 53
  play_pattern_timed [:g4, :g5, :fs5, :fs4, :e4, :g4, :e4, :e5], [e, e, e, e, e, e, e, e]   # bar 54
  play_pattern_timed [:fs5, :fs4, :e4, :e5, :d5, :d4, :cs4, :cs5], [e, e, e, e, e, e, e, e]   # bar 55
  play_pattern_timed [:b4, :b5, :a5, :a4, :g4, :e5, :a4, :a4], [e, e, e, e, de, s, e, e]   # bar 56
  play_pattern_timed [:a4, :r, :r], [q, q, h]   # bar 57
end

# Violin 2
in_thread do
  use_synth :piano
  # Second Violin by Keren Simpkins 
    play_pattern_timed [:r, :r, :r, :r], [w,w,w,w]  # bar 1-4
    #sleep 16  # bar 1-4
    play_pattern_timed [:fs5, :e5, :d5, :cs5], [q,q,q,q]  # bar 5
    play_pattern_timed [:b4, :a4, :b4, :cs5], [q,q,q,q]  # bar 6
    play_pattern_timed [:d5, :cs5, :b4, :a4], [q,q,q,q]  # bar 7
    play_pattern_timed [:g4, :fs4, :g4, :e4], [q,q,q,q]  # bar 8
    play_pattern_timed [:d4, :fs4, :a4, :g4, :fs4, :d4, :fs4, :e4], [e]  # bar 9
    play_pattern_timed [:d4, :b3, :d4, :a4, :g4, :b4, :a4, :g4], [e]  # bar 10
    play_pattern_timed [:fs4, :d4, :e4, :cs5, :d5, :fs5, :a5, :a4], [e]  # bar 11
    play_pattern_timed [:b4, :g4, :a4, :fs4, :d4, :d5, :d5, :cs5], [e, e, e, e, e, e, de, s]  # bar 12
    play_pattern_timed [:d5, :cs5, :d5, :d4, :cs4, :a4, :e4, :fs4, :d4, :d5, :cs5, :b4, :cs5, :fs5, :a5, :b5], [s]  # bar 13
    play_pattern_timed [:g5, :fs5, :e5, :g5, :fs5, :e5, :d5, :cs5, :b4, :a4, :g4, :fs4, :e4, :g4, :fs4, :e4], [s]  # bar 14
    play_pattern_timed [:d4, :e4, :fs4, :g4, :a4, :e4, :a4, :g4, :fs4, :b4, :a4, :g4, :a4, :g4, :fs4, :e4], [s]  # bar 15
    play_pattern_timed [:d4, :b3, :b4, :cs5, :d5, :cs5, :b4, :a4, :g4, :fs4, :e4, :b4, :a4, :b4, :a4, :g4], [s]  # bar 16
    play_pattern_timed [:fs4, :fs5, :e5, :r, :d5, :fs5], [e, e, q]  # bar 17
    play_pattern_timed [:b5, :a5, :b5, :cs6], [q]  # bar 18
    play_pattern_timed [:d6, :d5, :cs5, :r, :b4, :d5], [e, e, q]  # bar 19
    play_pattern_timed [:d5, :d5, :d5, :g5, :e5, :a5], [dq, e, e, e, e, e]  # bar 20
    play_pattern_timed [:a5, :fs5, :g5, :a5, :fs5, :g5, :a5, :a4, :b4, :cs5, :d5, :e5, :fs5, :g5, :fs5, :d5, :e5, :fs5, :fs4, :g4, :a4, :b4, :a4, :g4, :a4, :fs4, :g4, :a4], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]  # bar 21
    play_pattern_timed [:g4, :b4, :a4, :g4,:fs4, :e4, :fs4, :e4, :d4, :e4, :fs4, :g4, :a4, :b4, :g4, :b4, :a4, :b4, :cs5, :d5, :a4, :b4, :cs5, :d5, :e5, :fs5, :g5, :a5], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]  # bar 22
    play_pattern_timed [:fs5, :d5, :e5, :fs5, :e5, :d5, :e5, :cs5, :d5, :e5, :fs5, :e5, :d5, :cs5, :d5, :b4, :cs5, :d5, :d4, :e4, :fs4, :g4, :fs4, :e4, :fs4, :d5, :cs5, :d5], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]  # bar 23
    play_pattern_timed [:b4, :d5, :cs5, :b4, :a4, :g4, :a4, :g4, :fs4, :g4, :a4, :b4, :cs5, :d5, :b4, :d5, :cs5, :d5, :cs5, :b4, :cs5, :d5, :e5, :d5, :cs5, :d5, :b4, :cs5], [s, dsq, dsq, s, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq, dsq]  # bar 24
    play_pattern_timed [:d5, :r, :cs5, :r, :b4, :r, :d5, :r], [e]  # bar 25
    play_pattern_timed [:d4, :r, :d4, :r, :d4, :r, :e4, :r], [e]  # bar 26
    play_pattern_timed [:r, :a4, :r, :a4, :r, :fs4, :r, :a4], [e]  # bar 27
    play_pattern_timed [:r, :g4, :r, :fs4, :r, :g4, :r, :e5], [e]  # bar 28
    play_pattern_timed [:fs5, :fs4, :g4, :fs4, :e4, :e5, :fs5, :e5, :d5, :fs4, :d4, :b4, :a4, :a3, :g3, :a3], [s]  # bar 29
    play_pattern_timed [:b3, :b4, :cs5, :b4, :a4, :a3, :g3, :a3, :b3, :b4, :a4, :b4, :cs5, :cs4, :b3, :cs4], [s]  # bar 30
    play_pattern_timed [:d4, :d5, :e5, :d5, :cs5, :cs4, :d4, :cs4, :b3, :b4, :a4, :b4, :cs5, :cs4, :fs4, :e4], [s]  # bar 31
    play_pattern_timed [:d4, :d5, :e5, :g5, :fs5, :fs4, :a4, :fs5, :d5, :g5, :fs5, :g5, :e5, :a4, :g4, :a4], [s]  # bar 32
    play_pattern_timed [:fs4, :a4, :a4, :a4, :a4, :a4, :a4, :a4,:fs4, :fs4, :fs4, :fs4, :fs4, :fs4, :a4, :a4], [s]  # bar 33
    play_pattern_timed [:g4, :g4, :g4, :d5, :d5, :d5, :d5, :d5, :d5, :d5, :b4, :b4, :a4, :a4, :e5, :cs5], [s]  # bar 34
    play_pattern_timed [:a4, :fs5, :fs5, :fs5, :e5, :e5, :e5, :e5, :d5, :d5, :d5, :d5, :a5, :a5, :a5, :a5], [s]  # bar 35
    play_pattern_timed [:b5, :b5, :b5, :b5, :a5, :a5, :a5, :a5, :b5, :b5, :b5, :b5, :cs6, :cs5, :cs5, :cs5], [s]  # bar 36
    play_pattern_timed [:d5, :d4, :e4, :fs4, :d4, :cs4, :cs5, :d5, :e5, :cs5, :b4, :b3, :cs4, :d4, :b3, :cs4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, s]  # bar 37
    play_pattern_timed [:d4, :g4, :fs4, :e4, :g4, :fs4, :d4, :e4, :fs4, :a4, :g4, :b4, :a4, :g4, :fs4, :e4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, s]  # bar 38
    play_pattern_timed [:fs4, :d5, :cs5, :d5, :fs4, :a4, :a4, :b4, :cs5, :a4, :fs4, :d5, :e5, :fs5, :d5, :fs5, :fs5, :e5, :d5, :cs5], [s, dsq, dsq, s, s]  # bar 39
    play_pattern_timed [:b4, :b4, :a4, :b4, :cs5, :d5, :fs5, :e5, :d5, :fs5, :g5, :d5, :cs5, :b4, :b4, :a4, :e4, :a4, :a4], [s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, dsq, dsq, s, s, s, s, s, s]  # bar 40
    play_pattern_timed [:a4, :a4, :d4, :a4], [dq, e]  # bar 41
    play_pattern_timed [:g4, :a4, :g4, :d4, :d4, :cs4], [q, q, e, e, de, s]  # bar 42
    play_pattern_timed [:d4, :d5, :cs5, :b4, :a4], [e, e, q, q, q]  # bar 43
    play_pattern_timed [:d4, :e4, :fs4, :b4, :e4, :e4], [de, s, q, q, de, s]  # bar 44
    play_pattern_timed [:fs4, :fs5, :fs5, :g5, :fs5, :e5, :d5, :d5, :d5, :e5, :d5, :cs5], [de, s, s, s, s, s]  # bar 45
    play_pattern_timed [:b4, :d5, :d5, :c5, :b4, :c5, :a4, :a4], [q, q, s, s, s, s, de, s]  # bar 46
    play_pattern_timed [:a4, :a5, :a5, :b5, :a5, :g5, :fs5, :fs5, :fs5, :g5, :fs5, :e5], [de, s, s, s, s, s]  # bar 47
    play_pattern_timed [:d5, :c5, :b4, :c5, :a4, :a4, :g4, :d5, :cs5, :cs5], [s, s, s, s, de, s, e, e, de, s]  # bar 48
    play_pattern_timed [:d5, :d5, :cs5, :b4, :a4], [e, q, q, q, q]  # bar 49
    play_pattern_timed [:g4, :fs4, :e4, :e4], [q, 1.25, s, q]  # bar 50
    play_pattern_timed [:fs4, :fs5, :e5, :d5, :d6, :c6], [e, q, e]  # bar 51
    play_pattern_timed [:b5, :d6, :a5, :b5, :a5], [q, e, e, q, q]  # bar 52
    play_pattern_timed [:a5, :a4, :g4, :fs4, :fs5, :e5], [q, de, s]  # bar 53
    play_pattern_timed [:d5, :d5, :d5, :cs5], [dq, e, q, q]  # bar 54
    play_pattern_timed [:d5, :d4, :cs4, :cs5, :b4, :b3, :a3, :a4], [e]  # bar 55
    play_pattern_timed [:g4, :g5, :fs5, :fs4, :e4, :b4, :e4, :e5], [e]  # bar 56
    play_pattern_timed [:fs5, :r], [q, q, h]  # bar 57
end

# Violin 3
in_thread do
  use_synth :piano
  # Third violin by Xinhang Cecelia Sui
  
    play_pattern_timed [:r, :r, :r, :r, :r, :r], [w,w,w,w,w,w] # bar 1-6
    # play :r
    # sleep 24 # bar 1-6
    play_pattern_timed [:fs5, :e5, :d5, :cs5], [q] # bar 7
    play_pattern_timed [:b4, :a4, :b4, :cs5], [q,q,q,q] # bar 8
    play_pattern_timed [:d5, :cs5, :b4, :a4], [q,q,q,q] # bar 9
    play_pattern_timed [:g4, :fs4, :g4, :e4], [q,q,q,q] # bar 10
    play_pattern_timed [:d4, :fs4, :a4, :g4, :fs4, :d4, :fs4, :d4], [e] # bar 11
    play_pattern_timed [:d4, :a3, :d4, :a4, :g4, :b4, :a4, :g4], [e,e,e,e,e,e,e,e] # bar 12
    play_pattern_timed [:fs4, :d4, :e4, :cs5, :d5, :fs5, :a5, :a4], [e,e,e,e,e,e,e,e] # bar 13
    play_pattern_timed [:b4, :g4, :a4, :fs4, :d4, :d5, :d5, :cs5], [e, e, e, e, e, e, de, s] # bar 14
    play_pattern_timed [:d5, :cs5, :d5, :d4, :cs4, :a4, :e4, :fs4, :d4, :d5, :cs5, :b4, :cs5, :fs5, :a5, :b5], [s] # bar 15
    play_pattern_timed [:g5, :fs5, :e5, :g4, :fs5, :e5, :d5, :cs5, :b4, :a4, :g4, :fs4, :e4, :g4, :fs4, :e4], [s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s] # bar 16
    play_pattern_timed [:d4, :e4, :fs4, :g4, :a4, :e4, :a4, :g4, :fs4, :b4, :a4, :g4, :a4, :g4, :fs4, :e4], [s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s] # bar 17
    play_pattern_timed [:d4, :b3, :b4, :cs5, :d5, :cs5, :b4, :a4, :g4, :fs4, :e4, :b4, :a4, :b4, :a4, :g4], [s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s] # bar 18
    play_pattern_timed [:fs4, :fs5, :e5, :r, :d5, :fs5], [e, e, q, e, e, q] # bar 19
    play_pattern_timed [:b5, :a5, :b5, :cs6], [q,q,q,q] # bar 20
    play_pattern_timed [:d6, :d5, :cs4, :r, :d5, :fs5], [e, e, q, e, e, q] # bar 21
    play_pattern_timed [:d5, :d5, :d5, :g5, :e5, :a5], [dq, e, e, e, e, e] # bar 22
    play_pattern_timed [:a5, :fs5, :g5, :a5, :fs5, :g5], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:a5, :a4, :b4, :cs5, :d5, :e5, :fs5, :g5], [dsq, dsq,dsq,dsq,dsq,dsq,dsq,dsq]
    play_pattern_timed [:fs5, :d5, :e5, :fs5, :d4, :g4], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:a4, :b4, :a4, :g4, :a4, :fs4, :g4, :a4], [dsq,dsq,dsq,dsq,dsq,dsq,dsq,dsq] # bar 23
    play_pattern_timed [:g4, :b4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:fs4, :e4, :d4, :e4, :fs4, :g4, :a4, :b4], [dsq,dsq,dsq,dsq,dsq,dsq,dsq,dsq]
    play_pattern_timed [:g4, :b4, :a4, :b4, :cs5, :d5], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:a4, :b4, :cs5, :d5, :e5, :fs5, :g5, :a5], [dsq,dsq,dsq,dsq,dsq,dsq,dsq] # bar 24
    play_pattern_timed [:fs5, :d5, :e5, :fs5, :e5, :d5], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:e5, :cs5, :d5, :e5, :fs5, :e5, :d5, :cs5], [dsq, dsq, dsq, dsq,dsq, dsq,dsq, dsq]
    play_pattern_timed [:d5, :b4, :cs5, :d5, :d4, :e4], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:fs4, :g4, :fs4, :e4, :fs4, :d5, :cs5, :d5], [dsq,dsq, dsq, dsq,dsq, dsq,dsq, dsq] # bar 25
    play_pattern_timed [:b4, :d5, :cs5, :b4, :a4, :g4], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:a4, :g4, :fs4, :g4, :a4, :b4, :cs5, :d5], [dsq,dsq, dsq, dsq,dsq, dsq,dsq, dsq]
    play_pattern_timed [:b4, :d5, :cs5, :d5, :cs5, :b4], [s, dsq, dsq, s, dsq, dsq]
    play_pattern_timed [:cs5, :d5, :e5, :d5, :cs5, :d5, :b4, :cs5], [dsq] # bar 26
    play_pattern_timed [:d5, :r, :cs5, :r, :b4, :r, :d5, :r], [e,e,e,e,e,e,e,e] # bar 27
    play_pattern_timed [:d4, :r, :d4, :r, :d4, :r, :e4, :r], [e,e,e,e,e,e,e,e] # bar 28
    play_pattern_timed [:r, :a4, :r, :a4, :r, :fs4, :r, :a4], [e,e,e,e,e,e,e,e] # bar 29
    play_pattern_timed [:r, :g4, :r, :fs4, :r, :g4, :r, :e5], [e,e,e,e,e,e,e,e] # bar 30
    play_pattern_timed [:fs5, :fs4, :g4, :fs4, :e4, :e5, :fs5, :e5], [s,s,s,s,s,s,s,s]
    play_pattern_timed [:d5, :fs4, :d4, :b4, :a4, :a3, :g3, :a3], [s,s,s,s,s,s,s,s] # bar 31
    play_pattern_timed [:b3, :b4, :cs5, :b4, :a4, :a3, :g3, :a3], [s,s,s,s,s,s,s,s]
    play_pattern_timed [:b3, :b4, :a4, :b4, :cs5, :cs4, :b3, :cs4], [s,s,s,s,s,s,s,s] # bar 32
    play_pattern_timed [:d4, :d5, :e5, :d5, :cs5, :cs4, :d4, :cs4], [s,s,s,s,s,s,s,s]
    play_pattern_timed [:b3, :b4, :a4, :b4, :cs5, :cs4, :fs4, :e4], [s,s,s,s,s,s,s,s] # bar 33
    play_pattern_timed [:d4, :d5, :e5, :g5, :fs5, :fs4, :a4, :fs5], [s,s,s,s,s,s,s,s]
    play_pattern_timed [:d5, :g5, :fs5, :g5, :e5, :a4, :g4, :a4], [s,s,s,s,s,s,s,s] # bar 34
    play_pattern_timed [:fs4, :a4, :a4, :a4, :a4, :a4, :a4, :a4, :fs4, :fs4, :fs4, :fs4, :fs4, :fs4, :a4, :a4], [s,s,s,s,s,s,s,s,e,e,e,e,e,e,e,e] # bar 35
    play_pattern_timed [:g4, :g4, :g4, :d5, :d5, :d5, :d5, :d5, :d5, :d5, :b4, :b4, :a4, :a4, :e5, :cs5], [s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s] # bar 36
    play_pattern_timed [:a4, :fs5, :fs5, :fs5, :e5, :e5, :e5, :e5, :d5, :d5, :d5, :d5, :a5, :a5, :a5, :a5], [s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s] # bar 37
    play_pattern_timed [:b5, :b5, :b5, :b5, :a5, :a5, :a5, :a5, :b5, :b5, :b5, :b5, :cs6, :cs5, :cs5, :cs5], [s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s] # bar 38
    play_pattern_timed [:d5, :d4, :e4, :fs4, :d4], [s, dsq, dsq, s, s]
    play_pattern_timed [:cs4, :cs5, :d5, :e5, :cs5], [s, dsq, dsq, s, s]
    play_pattern_timed [:b4, :b3, :cs4, :d4, :b3], [s, dsq, dsq, s, s]
    play_pattern_timed [:cs4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, s]  # bar 39
    play_pattern_timed [:d4, :g4, :fs4, :e4, :g4], [s, dsq, dsq, s, s]
    play_pattern_timed [:fs4, :d4, :e4, :fs4, :a4], [s, dsq, dsq, s, s]
    play_pattern_timed [:g4, :b4, :a4, :g4, :fs4], [s, dsq, dsq, s, s]
    play_pattern_timed [:e4, :a4, :g4, :fs4, :e4], [s, dsq, dsq, s, s] # bar 40
    play_pattern_timed [:fs4, :d5, :cs5, :d5, :fs4], [s, dsq, dsq, s, s]
    play_pattern_timed [:a4, :a4, :b4, :cs5, :a4], [s, dsq, dsq, s, s]
    play_pattern_timed [:fs4, :d5, :e5, :fs5, :d5], [s, dsq, dsq, s, s]
    play_pattern_timed [:fs5, :fs5, :e5, :d5, :cs5], [s, dsq, dsq, s, s] # bar 41
    play_pattern_timed [:b4, :b4, :a4, :b4, :cs5], [s, dsq, dsq, s, s]
    play_pattern_timed [:d5, :fs5, :e5, :d5, :fs5], [s, dsq, dsq, s, s]
    play_pattern_timed [:g5, :d5, :cs5, :b4, :b4], [s, dsq, dsq, s, s]
    play_pattern_timed [:a4, :e4, :a4, :a4], [s,s,s,s] # bar 42
    play_pattern_timed [:a4, :e4, :d4, :a4], [dq, e, dq, e] # bar 43
    play_pattern_timed [:g4, :a4, :g4, :d4, :d4, :cs4], [q, q, e, e, de, s] # bar 44
    play_pattern_timed [:d4, :d5, :cs5, :b4, :a4], [e, e, q, q, q] # bar 45
    play_pattern_timed [:d4, :e4, :fs4, :b4, :e4, :e4], [de, s, q, q, de, s] # bar 46
    play_pattern_timed [:fs4, :fs5, :fs5, :g5, :fs5, :e5], [de, s, s, s, s, s]
    play_pattern_timed [:d5, :d5, :d5, :e5, :d5, :cs5], [de, s, s, s, s, s] # bar 47
    play_pattern_timed [:b4, :d5, :d5, :c5, :b4, :c5, :a4, :a4], [q, q, s, s, s, s, de, s] # bar 48
    play_pattern_timed [:a4, :a5, :a5, :b5, :a5, :g5], [de, s, s, s, s, s]
    play_pattern_timed [:fs5, :fs5, :fs5, :g5, :fs5, :e5], [de, s, s, s, s, s] # bar 49
    play_pattern_timed [:d5, :c5, :b4, :c5, :a4, :a4], [s, s, s, s, de, s]
    play_pattern_timed [:g4, :d5, :cs5, :cs5], [e, e, de, s] # bar 50
    play_pattern_timed [:d5, :d5, :cs5, :b4, :a4], [e, q, q, q, e] # bar 51
    play_pattern_timed [:a4, :g4, :fs4, :e4, :e4], [e, q, e+de, s, q] # bar 52
    play_pattern_timed [:fs4, :fs5, :e5, :d5, :d6, :c6], [e, q, e, e, q, e] # bar 53
    play_pattern_timed [:b5, :d6, :a5, :b5, :a5], [q, e, e, q, q] # bar 54
    play_pattern_timed [:a5, :a4, :g4, :fs4, :fs5, :e5], [q, de, s, q, de, s] # bar 55
    play_pattern_timed [:d5, :d5, :d5, :cs5], [dq, e, q, q] # bar 56
    play_pattern_timed [:d5, :r, :r], [q, q, h] # bar 57
end

in_thread do
  use_synth :pluck 
  #  Basso Continuo by Chen Zhu
  ## the 8-note sequence is repeated 28 times (in the first 56 bars)!
  in_thread do
    # use_synth :saw
    28.times do
      play_pattern_timed [:d3, :a2, :b2, :fs2], [q, q, q, q]   # bar 1 
      play_pattern_timed [:g2, :d2, :g2, :a2], [q, q, q, q]   # bar 2
    end
  
    # final bar  
    play_pattern_timed [:d2, :r, :r], [q, q, h]  # bar 57 
  end
  
  #---------------------------
  # chordal accompaniment 
  # algorithmically realisation of chordal accompaniment
  
  in_thread do
    28.times do
      random_invert = [0,1,2].choose
      play (chord :d4, :major, invert: random_invert) #note1: I - D major
      sleep 1
      play (chord :a3, :major, invert: random_invert) #note2: V - A major
      sleep 1
      play (chord :b3, :minor, invert: random_invert) #note3: vi - B minor
      sleep 1
      play (chord :fs3, :minor, invert: random_invert) #note4: iii - F# minor
      sleep 1
      play (chord :g3, :major, invert: random_invert) #note5: IV - G major
      sleep 1
      play (chord :d3, :major, invert: random_invert) #note6: I - D major
      sleep 1
      play (chord :g3, :major, invert: random_invert) #note7: IV - G major
      sleep 1
      play (chord :a3, :major, invert: random_invert) #note8: V - A major
      sleep 1
    end
  
    # final bar
    play (chord :d3, :major, invert: 0), release: q+q+h # bar 57
    sleep q+q+h
  end
  
end
