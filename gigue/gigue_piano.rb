# -*- mode: sonic-pi -*-
# Pachelbel: Gigue (2nd part of Canon and Gigue in D)
#    _______                     _          ____ 
#   / ____(_)___ ___  _____     (_)___     / __ \
#  / / __/ / __ `/ / / / _ \   / / __ \   / / / /
# / /_/ / / /_/ / /_/ /  __/  / / / / /  / /_/ / 
# \____/_/\__, /\__,_/\___/  /_/_/ /_/  /_____/  
#        /____/    
                              
# -----------------------/////////////////
# Kanon und Gigue für 3 Violinen mit Generalbaß
# Canon and Gigue for 3 violions with basso continuo
# Johann Pachelbel (1653-1706)
# music score based on Max Seiffert (1868-1948)'s edition (public domain) and Jawher Matmati's edition (CC-BY-NC-SA 4.0)
# source: https://imslp.org/wiki/Canon_and_Gigue_in_D_major%2C_P.37_(Pachelbel%2C_Johann)
# Sonic Pi code by Chen W. Zhu, Jessica Duckworth, Keren Simpkins and Cecelia Xinhang Sui (College of Arts and Law, University of Birmingham) July 2023
# Code licence: GNU General Public License 3.0
# /////////////////---------------------------

longa = 16 # long; c0000 (lilypond only) ==>  c\longa
breve = 8 # double whole; breve; c00 (lilypond only) ==> c\breve
dw = 6 # dotted whole note
w = 4 # whole note; semibreve
dh = 3 # dotted half note
h = 2 # half note; minim
dq = 1.5 # dotted quarter note
q = 1 # quarter note; crotchet
de = 0.75 # dotted eighth note
e = 0.5 # eighth note; quaver
s = 0.25 # sixteenth note; semiquaver
dsq = 0.125 # thirty second note; demisemiquiver semiquaver (I am using the british shorthand here)
t1 = 1.0/3 # triplet in 1 beats
t2 = 2.0/3 # triplet in 2 beats
t4 = 4.0/3 # triplet in 4 beats


use_bpm 150 # (100 x 1.5 beat) per minute

in_thread do
  use_synth :piano
  # gigue_violin_1
  # repeat once bars 1-11
  2.times do
    play_pattern_timed [:r], [w]   # bar 1
    play_pattern_timed [:r], [w]   # bar 2
    play_pattern_timed [:r, :d6, :cs6, :b5, :cs6, :a5], [dh, q, e, e, e, e]   # bar 3
    play_pattern_timed [:g5, :a5, :fs5, :e5, :fs5, :d5, :cs5, :d5, :e5], [e, e, e, e, e, e, dq, q, e]   # bar 4
    play_pattern_timed [:fs5, :g5, :e5, :d5, :e5, :cs5, :b4, :a4, :b4, :e4], [e, e, e, e, e, e, e, e, e, dq]   # bar 5
    play_pattern_timed [:e4, :r, :r, :r], [dq, q, e, dh]   # bar 6
    play_pattern_timed [:a5, :g5, :fs5, :g5, :e5, :d5, :e5, :cs5, :b4, :cs5, :a4], [q, e, e, e, e, e, e, e, e, e, e]   # bar 7
    play_pattern_timed [:gs4, :e4, :gs4, :a4, :fs4, :a4, :b4, :gs4, :b4, :cs5, :a4, :cs5], [e, e, e, e, e, e, e, e, e, e, e, e]   # bar 8
    play_pattern_timed [:b4, :gs4, :b4, :cs5, :a4, :cs5, :b4, :gs4, :cs5, :b4], [e, e, e, e, e, e, e, e, e, dq]   # bar 9
    play_pattern_timed [:cs5, :a4, :d5, :cs5, :b4, :cs5], [e, e, e, q, e, dh]   # bar 10
  end
  
  # repeat once bars 11-20
  2.times do
    play_pattern_timed [:r], [w]   # bar 11
    play_pattern_timed [:r], [w]   # bar 12
    play_pattern_timed [:r], [w]   # bar 13
    play_pattern_timed [:d5, :a5, :fs5, :e5, :fs5, :d5, :a4, :d5, :fs5, :d5, :fs5], [q, e, de, s, e, e, e, e, e, e, e]   # bar 14
    play_pattern_timed [:g5, :fs5, :e5, :d5, :cs5, :fs4, :d5, :cs5], [q, e, q, e, e, e, e, dq]   # bar 15
    play_pattern_timed [:b4, :b4, :e5, :cs5, :b4, :cs5, :d5], [dq, q, e, de, s, e, dq]   # bar 16
    play_pattern_timed [:d5, :g5, :e5, :d5, :e5, :fs5, :a5], [q, e, de, s, e, dq, dq]   # bar 17
    play_pattern_timed [:a5, :cs6, :a5, :cs6, :d6, :b5, :d6, :cs6, :a5, :cs6], [dq, e, e, e, e, e, e, e, e, e]   # bar 18
    play_pattern_timed [:d6, :b5, :d6, :cs6, :a5, :cs6, :d6, :b5, :d6, :d6, :cs6], [e, e, e, e, e, e, e, e, e, q, e]   # bar 19
    play_pattern_timed [:d6, :d6, :cs6, :d6], [dq, q, e, dh]   # bar 20
  end
  
end

in_thread do
  use_synth :piano 
  # Gigue: violin 2
  2.times do
    sleep 12  # bar 1
    play_pattern_timed [:a5, :g5, :fs5, :g5, :e5, :d5, :e5, :cs5, :b4, :cs5, :a4], [q, e, e, e, e, e, e, e, e, e, e]  # bar 2
    play_pattern_timed [:gs4, :a4, :b4, :g4, :fs4, :g4, :a4, :b4, :cs5], [dq, e, e, e, e, e, e, q, e]  # bar 3
    play_pattern_timed [:d5, :d5, :b4, :a4, :a4, :g4], [q, e, dq, dq, q, e]  # bar 4
    play_pattern_timed [:fs4, :r, :e5, :d5, :cs5, :d5, :b4], [dq, dq, q, e, e, e, e]  # bar 5
    play_pattern_timed [:a4, :b4, :g4, :fs4, :g4, :e4, :d4, :e4, :fs4, :e4, :fs4, :d4], [e]  # bar 6
    play_pattern_timed [:cs4, :d4, :e4, :d4, :d5, :cs5, :b4, :cs5, :d5, :e5, :cs5], [e, e, e, e, e, e, q, e, e, e, e]  # bar 7
    play_pattern_timed [:b4, :gs4, :b4, :cs5, :a4, :cs5, :gs4, :e4, :gs4, :a4, :fs4, :a4], [e]  # bar 8
    play_pattern_timed [:gs4, :e4, :gs4, :a4, :fs4, :a4, :gs4, :e4, :gs4, :a4, :gs4], [e, e, e, e, e, e, e, e, e, q, e]  # bar 9
    play_pattern_timed [:a4, :a4, :gs4, :a4], [dq, q, e, dw]  # bar 10
  end
  
  2.times do
    play_pattern_timed [:a4, :e5, :cs5, :b4, :cs5, :a4, :e4, :a4, :cs5, :a4, :cs5], [q, e, de, s, e, e, e, e, e, e, e]  # bar 11
    play_pattern_timed [:d5, :e5, :d5, :e5, :fs5, :g5, :a5, :a4], [q, e]  # bar 12
    play_pattern_timed [:b4, :e5, :b4, :a4, :d5, :a4, :b4, :a4, :b4, :a4, :g4], [e, e, e, e, e, e, q, e, e, e, e]  # bar 13
    play_pattern_timed [:fs4, :g4, :a4, :a4, :a4], [q, e, dq, dq, dq]  # bar 14
    play_pattern_timed [:b4, :cs5, :b4, :as4, :b4, :cs5, :fs5], [dq, q, e, q, e, q, e]  # bar 15
    play_pattern_timed [:ds5, :cs5, :ds5, :e5, :e5, :a5, :fs5, :e5, :fs5], [de, s, e, dq, q, e, de, s, e]  # bar 16
    play_pattern_timed [:g5, :b5, :a5, :a5, :d6, :cs6, :b5, :cs6], [q, e, dq, q, e, de, s, e]  # bar 17
    play_pattern_timed [:a5, :e5, :a5, :e5, :cs5, :e5, :fs5, :d5, :fs5, :e5, :cs5, :e5], [e]  # bar 18
    play_pattern_timed [:fs5, :d5, :fs5, :e5, :cs5, :e5, :fs5, :d5, :fs5, :e5], [e, e, e, e, e, e, e, e, e, dq]  # bar 19
    play_pattern_timed [:fs5, :d5, :g5, :fs5, :e5, :fs5], [e, e, e, q, e, dw]
  end
end

in_thread do
  use_synth :piano
  # Gigue: Violion 3
  # repeat once bars 1-11
  2.times do
    play_pattern_timed [:d5, :cs5, :b4, :cs5, :a4, :g4, :a4, :fs4, :e4, :fs4, :d4], [q,e,e,e,e,e,e,e,e,e,e] # bar 1  
    play_pattern_timed [:cs4,:d4, :d5, :cs5, :b4, :cs5, :a4, :g4, :a4, :fs4], [dq,e,e,e,e,e,e,e,e,e] # bar 2
    play_pattern_timed [:e4, :fs4, :d4, :cs4, :d4, :e4, :fs4, :g4, :a4], [e, e, e, dq, e, e, e, q, e] # bar 3
    play_pattern_timed [:b4, :a4, :gs4, :a4, :g4, :fs4, :g4, :e4], [q, e, dq, q, e, e, e, e] # bar 4
    play_pattern_timed [:d4, :e4, :cs4, :b4, :cs4, :a3, :gs3, :a3, :b3], [e, e, e, e, e, e, dq, q, e] # bar 5
    play_pattern_timed [:cs4, :d4, :e4, :cs4, :b3, :cs4, :d4, :b3], [dq, e, e, e, e, e, e, dq] # bar 6
    play_pattern_timed [:a3, :a4, :g4, :fs4, :e4, :fs4], [dq, q, e, q, e, dq] # bar 7
    play_pattern_timed [:b3, :e4, :e4, :e4, :b3, :e4, :e4], [q, e, dq, e, e, e, dq] # bar 8
    play_pattern_timed [:e4, :b3, :e4, :e4, :e4, :e4], [e, e, e, dq, dq, dq] # bar 9
    play_pattern_timed [:e4, :fs4, :e4, :d4, :e4], [q, e, q, e, dh] # bar 10
  end
  
  # repeat once bars 11-20
    2.times do  
  play_pattern_timed [:r], [dw] # bar 11
    play_pattern_timed [:cs4,:a4, :fs4, :e4, :fs4, :d4, :a3, :d4, :fs4, :d4, :fs4], [q,e,e,e,e,e,e,e,e,e,e] # bar 12
    play_pattern_timed [:g4, :e4, :g4, :fs4, :d4, :fs4, :g4, :fs4, :g4, :fs4, :e4], [e, e, e, e, e, e, q, e, e, e, e] # bar 13
    play_pattern_timed [:d4, :e4, :d4, :fs4, :d4], [q, e, dq, dq, dq] # bar 14
    play_pattern_timed [:d4, :a4, :fs4, :fs4, :b4, :b4, :as4], [dq, q, e, q, e, q, e] # bar 15
    play_pattern_timed [:b4, :fs4, :b4, :gs4, :fs4, :gs4, :a4, :a4, :d5], [e, e, e, de, s, e, dq, q, e] # bar 16
    play_pattern_timed [:b4, :a4, :b4, :cs5, :b4, :cs5, :d5, :e5], [de, s, e, de, s, e, dq, dq] # bar 17
    play_pattern_timed [:cs5,:a4, :a3, :a4, :a4, :a3, :a4, :a4, :a3, :a4], [dq,e,e,e,e,e,e,e,e,e] # bar 18
    play_pattern_timed [:a4, :a3, :a4, :a4, :a3, :a4, :a4, :a3, :a4, :a4, :a3, :a4], [e,e,e,e,e,e,e,e,e,e,e,e] # bar 19
    play_pattern_timed [:a4,:b4,:a4,:g4,:a4], [q,e,q,e,dh] # bar 20
  end
  
end

in_thread do
  use_synth :pluck
  # Gigue: basso continuo
  # time signature: 12/8  
  
  # bars 1-10: repeat once 
  2.times do
    play_pattern_timed [:d3,:g3,:fs3,:e3,:d3,:cs3,:b2], [dq, q, e, q,e,q,e] #bar1 
    play_pattern_timed [:a2,:b2,:g2,:d2],[dq,dq,dq,dq]  #bar2  
    play_pattern_timed [:e3,:a2,:d3,:g2], [dq,dq,dq,dq] #bar3
    play_pattern_timed [:g2,:d2,:e2,:a2,:d3,:g2],[q,e,dq,dq,q,e] #bar4
    play_pattern_timed [:d3,:d3,:e3,:b2,:a2],[dq,dq,q,e,dq] #bar5
    play_pattern_timed [:a2,:d3,:g3,:d3,:gs2],[dq,dq,q,e,dq] #bar6
    play_pattern_timed [:a2,:b2,:cs3,:d3,:d2,:d2],[e,e,e,dq,dq,dq] #bar7
    play_pattern_timed [:d2,:d2,:d2,:d2],[dq,dq,dq,dq] #bar8
    play_pattern_timed [:d2,:d2,:d2,:d2],[dq,dq,dq,dq] #bar9
    play_pattern_timed [:a2,:a2,:a2],[dq,dq,dh] #bar10
  end
  
  # bars 11-20: repeat once 
  2.times do
    play_pattern_timed [:a2,:gs2,:a2,:a2,:a2],[q,e,dq,dq,dq] #bar11
    play_pattern_timed [:d3,:cs3,:d3,:d3,:d3],[q,e,dq,dq,dq] #bar12
    play_pattern_timed [:g2,:d3,:g2,:d3,:g2,:a2],[dq,dq,q,e,q,e] #bar13
    play_pattern_timed [:d3,:cs3,:d3,:d3,:d3],[q,e,dq,dq,dq] #bar14
    play_pattern_timed [:g2,:a2,:b2,:fs2,:fs2],[dq,q,e,dq,dq] #bar15
    play_pattern_timed [:b2,:e3,:a2,:d3],[dq,dq,dq,dq] #bar16
    play_pattern_timed [:g2,:a2,:d3,:a2],[dq,dq,dq,dq] #bar17
    play_pattern_timed [:a2,:a2,:a2,:a2],[dq,dq,dq,dq] #bar18
    play_pattern_timed [:a2,:a2,:a2,:a2],[dq,dq,dq,dq] #bar19
    play_pattern_timed [:d2,:d2,:d2],[dq,dq,dh] #bar20
  end
  
end
