# DOI 
10.5281/zenodo.10051787

# Sonic Canon: Coding Pachelbel’s Canon and Gigue with Sonic Pi
This repo contains Pachelbel’s *Canon and Gigue in D* coded in the music programming language Sonic Pi (https://sonic-pi.net/). We are thankful to developers of Sonic Pi, which is incredibly versatile and fun to use.

Coders are Chen Zhu (https://cwzhu.gitlab.io/) and three 1st-year undegraduate students–Jessica Duckworth (BMus), Keren Simpkins (BMus) and Cecelia Xinhang Sui(LLB)–from the College of Arts and Law, University of Birmingham. 


# About the piece
- Title (German): Kanon und Gigue für 3 Violinen mit Generalbaß
- Title (English): Canon and Gigue for 3 violions with basso continuo
- Composer: Johann Pachelbel (1653-1706)

- Music score based on Max Seiffert (1868-1948)'s edition (public domain) and Jawher Matmati's edition (CC-BY-NC-SA 4.0)
- score source: https://imslp.org/wiki/Canon_and_Gigue_in_D_major%2C_P.37_(Pachelbel%2C_Johann)

# Music
There can be an unlimited number of ways of “playing” the coded piece by experimenting with different built-in instruments known as “synths”. (https://github.com/sonic-pi-net/sonic-pi/blob/dev/etc/doc/cheatsheets/synths.md) Please feel free to change them. At the moment, this repo contains these instrumental variations:

- Canon: piano version (Piano + Pluck)
- Canon: SSTP version (Sine, Square, Triangle, Pluck)
- Gigue: piano version
- Gigue: saw version

# Contributors
- Chen W. Zhu, Jessica Duckworth, Keren Simpkins and Cecelia Xinhang Sui (College of Arts and Law, University of Birmingham)
- July 2023


# Shorthand for note durations (not all used)
We use variables for naming note durations. This saves us some time from typing a lot of ```0.5```s, ```0.25 ```s etc. 

```{sonic-pi}
longa = 16 # long
breve = 8 # double whole; breve;
dw = 6 # dotted whole note
w = 4 # whole note; semibreve
dh = 3 # dotted half note
h = 2 # half note; minim
dq = 1.5 # dotted quarter note
q = 1 # quarter note; crotchet
de = 0.75 # dotted eighth note
e = 0.5 # eighth note; quaver
s = 0.25 # sixteenth note; semiquaver
dsq = 0.125 # thirty second note; demisemiquiver semiquaver (british shorthand)
```
# Sonic Pi’s buffer limit
It is worth noting Sonic Pi has a buffer size limit for a maximum number of lines of code that can be played. Once a buffer hits this limit, Sonic Pi will not be able to run the code (with no error warning!). See the discussion here: https://in-thread.sonic-pi.net/t/maximum-characters-limit-per-buffer/229/3

One workaround is to use the ```run_file```​​​ command that plays a saved file. One needs to spell out the location and the file name by typing something like this:​ ```run_file  /PATH/canon_piano.rb`` (Please, for example, change /PATH/canon_pinao.rb to the actual location of the saved file accordingly).

The ruby files for Pachelbel’s *Canon* in this repo are a bit too long (~300 lines) to be played in a single Sonic Pi buffer. So it is necessary to use the ```run_file```​​ method.​

# Code License
GNU General Public License 3.0 (GPLv3)
